--version
---------

``--version`` displays version information, then exit.

Short Option
~~~~~~~~~~~~

``-v``

Example
~~~~~~~

.. code-block:: bash

	java -jar jaqy.jar -v
